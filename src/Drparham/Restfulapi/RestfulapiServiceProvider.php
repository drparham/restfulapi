<?php namespace Drparham\Restfulapi;

use Illuminate\Support\ServiceProvider;


class RestfulapiServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('drparham/restfulapi');
		\App::register('LucaDegasperi\OAuth2Server\OAuth2ServerServiceProvider');
		\App::register('Barryvdh\Cors\CorsServiceProvider');
		include __DIR__.'/../../../routes.php';

		/**=========================
		 * register all API Interface to inject the correct API into controllers
		 *===========================*/

		//explode URI to see if it contains a v#.
		if(isset($_SERVER['REQUEST_URI'])){
			$uri = explode('/',$_SERVER['REQUEST_URI']);
		}	

		if(isset($uri[2])){
			\App::bind('API\Resource\\'.$uri[2].'\\ResourceAPIInterface', function() use ($uri){
				$resource = ucfirst($uri[3]);
				// if($uri[3]=='credittype'){
				// 	$resource='CreditType';
				// }elseif($uri[3]=='coursemenu'){
				// 	$resource='CourseMenu';
				// }elseif($uri[3]=='sidemenu'){
				// 	$resource='SideMenu';
				// }else {
				// 	$resource = ucfirst($uri[3]);
				// }

				$version = $uri[2];	
				return \API\Factory::getInstance($resource, $version);
			});
		}


		\App::error(function(\Exception $exception, $code)
		{
			\Log::error($exception);
		    switch ($code){
		    	case 400:    	
				    $response = array(
				    	'status'=>400,
				    	'error'=>'Bad Request',
				    	'error_message'=>'The request could not be understood by the server due to malformed syntax'
				    	);
				    return \Response::make(json_encode($response),403);

		        case 403:
		            $response = array(
				    	'status'=>403,
				    	'error'=>'Forbidden',
				    	'error_message'=>'Access to this resource is forbidden'
				    	);
				    return \Response::make(json_encode($response),403);

		        case 404:
				    $response = array(
				    	'status'=>404,
				    	'error'=>'Resource not found',
				    	'error_message'=>'These are not the resources you are looking for'
				    	);
				    return \Response::make(json_encode($response),404);

				case 410:
					$response = array(
				    	'status'=>410,
				    	'error'=>'410 Gone',
				    	'error_message'=>'The requested resource is no longer available at the server and no forwarding address is known'
				    	);
				    return \Response::make(json_encode($response),404);

		      //   case 500:
			    	// $response = array(
			    	// 'status'=>500,
			    	// 'error'=>'Server Error',
			    	// 'error_message'=>'There was an unexpected condition encountered. Unable to give a more specific response'
			    	// );
			    	// return \Response::make(json_encode($response),500);

				case 503:
		        	 $response = array(
				    	'status'=>503,
				    	'error'=>'Resource currently unavailable',
				    	'error_message'=>'The server is currently unable to respond to your request, please check back later'
				    	);
		    		return \Response::make(json_encode($response),503);
		    	

		        // default:
		        // 	$response = array(
		        // 		'status'=>$code,
		        // 		'error'=>$code,
		        // 		'error_message'=>$exception
		        // 		);
		        //    	return \Response::make(json_encode($response),$code);
		    }
		});

	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
	  	$this->app['restfulapi'] = $this->app->share(function($app)
		{
			return new Restfulapi;
		});


	  	$this->app->booting(function()
		{
			$loader = \Illuminate\Foundation\AliasLoader::getInstance();
		  	$loader->alias('Restfulapi', 'Drparham\Restfullapi\Facades\Restfulapi');
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
