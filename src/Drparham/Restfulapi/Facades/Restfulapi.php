<?php namespace Drparham\Restfulapi\Facades;
 
use Illuminate\Support\Facades\Facade;
 
class Restfulapi extends Facade {
 
  /**
   * Get the registered name of the component.
   *
   * @return string
   */
  protected static function getFacadeAccessor() { return 'restfulapi'; }
 
}