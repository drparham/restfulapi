<?php
namespace API;

/**
 * API interface that all API Interfacess should implement.  Add any function here that
 * should be requried of any API Interface
 *
 * @author  Dustin Parham <dr.parham@gmail.com>
 */
interface APIInterface {
    
    /**
     * Gets the API version
     */
    public static function version();

    /**
     * Return Name of API
     * @return string Name of API
     */
    public static function name();
    
}
?>