<?php
namespace API;



/**
 * API Factory creates the correct API/version to inject into controller
 *
 * @author  Dustin Parham <dr.parham@gmail.com>
 */
Class Factory {

	const NS = "API\\";
    
    /**
     *
     * @param string $api
     * @param int $version
     * @return APIInterface; 
     */
    public static function getInstance($api, $version='current'){
        if($version == 'current'){
            $apiConfig = \Config::get('restfulapi::api');
            //Check if API resource exists
            if(isset($apiConfig[$api])){
                $version = $apiConfig[$api]['currentVersion'];
            }else {
                //Need to throw 404 error
                \App::abort(404);
            }
            $version = $apiConfig[$api]['currentVersion'];
        }else {
        	$apiConfig = \Config::get('restfulapi::api');
            $arr1 = str_split($version);
            //Check if API resource exists              
            if(isset($apiConfig[$api])){
                $arr2 = str_split($apiConfig[$api]['currentVersion']);
            }else {
                //Need to throw 404 error
                \App::abort(404);
            }

        	if((int)$arr1[1]  >  (int)$arr2[1]){
        		$version = $apiConfig[$api]['currentVersion'];
        	}
        }
        	
        $apiClass = self::NS.$api."\\".$version."\\".$api.'API';
        
        if (class_exists($apiClass)) {
            return \App::make($apiClass);
        }else {
            //Need to throw 404 error
            \App::abort(404);
        }
    }
}

?>