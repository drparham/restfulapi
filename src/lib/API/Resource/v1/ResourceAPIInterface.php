<?php
namespace API\Resource\v1;

use API\APIInterface;

/**
 * The Resource API interface.  All versions of the API should implement this interface.
 *
 * @author Dustin Parham <dr.parham@gmail.com>
 */
interface ResourceAPIInterface extends APIInterface {

    /**
     * Fetches a single item by id from the given $entity
     */
    public static function fetchOne($id, $data);

    /**
     * Fetches all records from a given $entity.
     */
    public static function fetchAll($data);

    /**
     * Deletes a single record by id from the given $entity
     */
    public static function deleteOne($id);

    /**
     * Toggle the active field on an given $entity and $id
     */
    public static function toggle($id, $fieldName);

    /**
     * Patch updates a field in a record set via the PATCH HTTP verb.
     * @param  int $id   Record to patch
     * @param  array $data Array of values required for patching a field in a record
     * @return array       Return array of values after being updated
     */
    public static function patch($id, $data);

    /**
     * Update/replace a record via the PUT HTTP verb.
     * @param  int $id   Record to patch
     * @param  array $data Array of key value pairs of data to update/replace
     * @return array       Return array of values after being updated
     */
    public static function update($id, $data);

    /**
     * Create a new record in the Course Table
     * @param  array $data Array of value key pairs to populate record with in table.
     * @return array       Return Array of values after bieng created
     */
    public static function create($data);
    /**
     * Open fetch interface
     */
    //public static function fetchBy($where, $bind=null, $cols=null, $limit=null, $skip=null, $order=null);
}

?>
