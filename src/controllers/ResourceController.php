<?php

use API\Resource\v1\ResourceAPIInterface;


class ResourceController extends \BaseController {

	/**
	 * Set access-control-allow-origin for OAuth2
	 */
	public function __construct(ResourceAPIInterface $api)
	{
	    $this->beforeFilter('oauth:basic');
	    $this->api = $api;
	    
	    $this->OwnerID = 4567898765;	   
	   
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($resource, $version)
	{
		$input = Input::all();
		//Populate Data
		$data = $this->api->fetchAll($input);
		
		//Trigger event 
		$event = array("name"=>$this->api->name(),"event"=>"FetchAll",'ownerId'=>$this->OwnerID);
        $eventresponse = Event::fire('log_event', array($event));
		
		//Format response
		if($data->isEmpty()){
			//Format response
			$response = Response::make(" ", 204);
        }else {
			//Format response
			$response = Response::make($data, 200);
        }

		//Set content-type in header
		$response->header('Content-Type', 'application/json');
		$response->header('Cache-Control', 'max-age=3600');
		return $response;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($resource, $version)
	{
		//

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($resource, $version)
	{	
		$this->beforeFilter('oauth:basic,admin');
		
		$data = Input::all();
		$course = $this->api->create($data);
		
		$event = array("name"=>$this->api->name(),"event"=>"store",'ownerId'=>$this->OwnerID,"resource_id"=>$course->id);
        $eventresponse = Event::fire('log_event', array($event));

		//Format response
		$response = Response::make($course, 201);

		//Set content-type in header
		$response->header('Content-Type', 'application/json');
		$response->header('Cache-Control', 'max-age=3600');
		return $response;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($resource, $version, $id=null)
	{	
		$this->beforeFilter('oauth:basic');
		
		$input = Input::all();

		//Populate Data
		$data = $this->api->fetchOne($id, $input);

		$event = array("name"=>$this->api->name(),"event"=>"show",'ownerId'=>$this->OwnerID,"resource_id"=>$id);
        $eventresponse = Event::fire('log_event', array($event));

        if(empty($data)){
			//Format response
			$response = Response::make(" ", 204);
        }else {
			//Format response
			$response = Response::make($data, 200);
        }

		//Set content-type in header
		$response->header('Content-Type', 'application/json');
		$response->header('Cache-Control', 'max-age=3600');
		return $response;
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($resource, $version, $id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($resource, $version, $id)
	{
		$this->beforeFilter('oauth:basic,admin');
		//Capture Data
		$data = Input::all();

		if (Request::isMethod('patch'))
		{
		    $course = $this->api->patch($id, $data);
		}else {
			$course = $this->api->update($id, $data);
		}
		$event = array("name"=>$this->api->name(),"event"=>"update",'ownerId'=>$this->OwnerID,"resource_id"=>$id);
        $eventresponse = Event::fire('log_event', array($event));

		//Format response
		$response = Response::make($data, 200);
		//Set content-type in header
		$response->header('Content-Type', 'application/json');
		return $response;

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($resource, $version, $id)
	{
		$this->beforeFilter('oauth:basic,admin');
		$course = $this->api->deleteOne($id);
		$event = array("name"=>$this->api->name(),"event"=>"delete",'ownerId'=>$this->OwnerID,"resource_id"=>$id);
        $eventresponse = Event::fire('log_event', array($event));

		//Format response
		$response = Response::make($course, 204);
		//Set content-type in header
		$response->header('Content-Type', 'application/json');
		return $response;

	}

	public function missingMethod($parameters = array())
	{
	    print_r($parameters);
	}

}