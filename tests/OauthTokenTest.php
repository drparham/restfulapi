<?php
class ExampleTest extends \Illuminate\Foundation\Testing\TestCase {
	
	/**
	 * Creates the application.
	 *
	 * @return \Symfony\Component\HttpKernel\HttpKernelInterface
	 */
	public function createApplication()
	{
		$unitTesting = true;

		$testEnvironment = 'testing';

		return require __DIR__.'/../../../../bootstrap/start.php';
	}
	
	/**
	 * Retrieve Token 
	 *
	 * @return void
	 */
	public function testGrantTypeToken()
	{
		$data = array(
			'grant_type'=>'client_credentials',
			'client_id'=>'4567898765',
			'client_secret'=>'89asdf8asdf87afsd8790asfd78afsd70',
			'scope'=>'basic'
			);
		// Now Up-vote something with id 53
		$this->client->request('POST', 'oauth/access_token', $data );

		// I hope we always get a 200 OK
		$this->assertTrue($this->client->getResponse()->isOk());

		// Get the response and decode it
		$jsonResponse = $this->client->getResponse()->getContent();
		$responseData = json_decode($jsonResponse);		
		$this->assertInternalType('string', $responseData->access_token);

	}
	
}