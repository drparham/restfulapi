<?php 

class ResourceControllerTest extends Illuminate\Foundation\Testing\TestCase {

	/**
	 * Creates the application.
	 *
	 * @return \Symfony\Component\HttpKernel\HttpKernelInterface
	 */
	public function createApplication()
	{
		$unitTesting = true;

		$testEnvironment = 'testing';

		return require __DIR__.'/../../../bootstrap/start.php';
	}

	public function setUp()
	{
		parent::setUp();
		
		//Artisan::call('migrate'); 
		//$this->seed();

		$this->mock = $this->mock('API\Resource\v1\ResourceAPIInterface');
	}


	public function mock($class)
	{
	  $mock = Mockery::mock($class);
	 
	  $this->app->instance($class, $mock);
	 
	  return $mock;
	}

	public function testIndex($resource="course", $version="v1")
	{
		// Arrange... 
		$course = Mockery::mock('Eloquent', 'API\Entity\v1\Page')->makePartial();
		//(object)array(array('id'=>1, 'name'=>'Widget-name','description'=>'Widget description'),array('id'=>2, 'name'=>'Widget-name2','description'=>'Widget description2'))
		$course->shouldReceive('isEmpty')->once()->andReturn(false);
		$course->shouldReceive('all')->once()->andReturn($course);
		$this->mock->shouldReceive('fetchAll')->once()->andReturn($course->all());

		$this->mock->shouldReceive('name')->once()->andReturn('Course');

		// Act... 
		$response = $this->action('GET', 'ResourceController@index'); 

		// Assert... 
		$this->assertResponseOk(); 


	}

	public function testshow($resource="course", $version="v1", $id=85)
	{
		// Arrange... 
		$course = Mockery::mock('Eloquent', 'API\Entity\v1\Page')->makePartial();

		$course->shouldReceive('find')->once()->andReturn((object)array('id'=>1, 'name'=>'Widget-name','description'=>'Widget description'));

		$this->mock->shouldReceive('fetchOne')->once()->andReturn(json_encode($course->find($id)));
		$this->mock->shouldReceive('name')->once()->andReturn('Course');

		// Act... 
		$response = $this->action('GET', 'ResourceController@show'); 

		// Assert... 
		$this->assertResponseOk(); 


	}

	public function testStore($resource="course", $version="v1")
	{
		//Test saving
		//arrange...

	}


	public function tearDown()
	{
	  Mockery::close();
	}
}