<?php

/*
|--------------------------------------------------------------------------
| Package Routes
|--------------------------------------------------------------------------
|
| Here are all of the routes for this package.
|
*/

Route::get('/login', function()
{	
	$params = Input::all();
	//$params = Session::get('authorize-params');

	if (Auth::check()){
		return Redirect::to('/oauth/authorize?'.http_build_query($params));
    }else {
		return View::make('login')->with('params',$params);
    }

});

Route::post('/login', function(){
	    $data = Input::all();

        if (Auth::attempt(array('username' => $data['username'], 'password' => $data['password'], 'active' => 1), Input::has('remember')))
        {
            return Redirect::to('/oauth/authorize?'.http_build_query($data));
        }else {
            return json_encode(array("errors"=> 1, "message"=>"Your username password combination is invalid. Please try again."));
        }
});

Route::get('/', function()
{
	return View::make('hello');
});

Route::post('oauth/access_token', array('as'=>'oauth/access_token', function()
{
    return  AuthorizationServer::performAccessTokenFlow();
}));

Route::get('/oauth/authorize', array('before' => 'check-authorization-params|auth', function()
{
    // get the data from the check-authorization-params filter
    $params = Session::get('authorize-params');

    // get the user id
    $params['user_id'] = Auth::user()->id;

    // display the authorization form
    
    return View::make('authorization-form', array('params' => $params));
}));

Route::post('/oauth/authorize', array('before' => 'check-authorization-params|auth|csrf', function()
{
    $input = Input::all();
    //print_r($input);

    // get the data from the check-authorization-params filter
    $params = Session::get('authorize-params');




    // get the user id
    $params['user_id'] = Auth::user()->id;

    // check if the user approved or denied the authorization request
    if (!empty($input['approve'])) {

        $code = AuthorizationServer::newAuthorizeRequest('user', $params['user_id'], $params);

        Session::forget('authorize-params');

        return Redirect::to(AuthorizationServer::makeRedirectWithCode($code, $params));
    }else if(!empty($input['deny'])) {

        Session::forget('authorize-params');

        return Redirect::to(AuthorizationServer::makeRedirectWithError($params));
    }else {
    	return "Error processing form";
    }
}));


/**
 * Route url with any v# to the resource controller, let IoC Dependency injection determine proper version of API to inject
 */
Route::group(array('prefix' => 'api/{v1}'), function()
{   
    // Manually define resource routes to catch all resource end points
    // Retrieve data
    Route::GET('{route}', array('as' => 'api.{v1}.{route}.index', 'uses' => 'ResourceController@index'));
    Route::GET('{route}/{id}', array('as' => 'api.{v1}.{route}.show', 'uses' => 'ResourceController@show'));
    // Retrive Forms for Create/Update
    Route::GET('{route}/create', array('as' => 'api.{v1}.{route}.create', 'uses' => 'ResourceController@create'));
    Route::GET('{route}/{id}/edit', array('as' => 'api.{v1}.{route}.edit', 'uses' => 'ResourceController@edit'));
    // Modify Existing Data Create/Update/Edit/Delete
    Route::POST('{route}', array('as' => 'api.{v1}.{route}.store', 'uses' => 'ResourceController@store'));
    Route::PUT('{route}/{id}', array('as' => 'api.{v1}.{route}.update', 'uses' => 'ResourceController@update'));
    Route::PATCH('{route}/{id}', 'ResourceController@update');
    Route::DELETE('{route}/{id}', array('as' => 'api.{v1}.{route}.destroy', 'uses' => 'ResourceController@destroy'));

});
