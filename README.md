[![Total Downloads](https://poser.pugx.org/drparham/restfulapi/downloads.svg)](https://packagist.org/packages/drparham/framework)
[![Latest Stable Version](https://poser.pugx.org/drparham/restfulapi/v/stable.svg)](https://packagist.org/packages/drparham/restfulapi/)
[![Latest Unstable Version](https://poser.pugx.org/drparham/restfulapi/v/unstable.svg)](https://packagist.org/packages/drparham/restfulapi)

restfulapi
==========

This is a Laravel 4 Package that adds OAuth2/CORS/Version Control, and end point management for adding a REST API to any Laravel 4 project.

### Note::

This package is currently an Alpha release. The package still needs to have test cases created and ran to verify correct functionality. This code is based off of a Laravel 4 implemention that is not in package format, and currently in pre-production use.

This package requires and installs the following packages for OAuth2 and CORS support:

https://github.com/lucadegasperi/oauth2-server-laravel

https://github.com/barryvdh/laravel-cors

Installation
============

Add the following line to your composer.json file under require:

    "drparham/restfulapi": "0.0.2-alpha"
  
Add this line of code to your providers array in config/app.php
		
    'Drparham\Restfulapi\RestfulapiServiceProvider',
	
Add the following lines to your alias array in config/app.php

    'AuthorizationServer' => 'LucaDegasperi\OAuth2Server\Facades\AuthorizationServerFacade',
    'ResourceServer' => 'LucaDegasperi\OAuth2Server\Facades\ResourceServerFacade',

Next Run the following command to update Composer's autoload class list:

    composer dump-autoload

Run the following commands to publish config files for you to customize the configs for your project:

    php artisan config:publish lucadegasperi/oauth2-server-laravel
    php artisan config:publish barryvdh/laravel-cors
    php artisan config:publish drparham/restfulapi
    
Run the following migrations to create the necessary DB structures for the OAUTH2 system:

    php artisan migrate --package="lucadegasperi/oauth2-server-laravel"
    
In your app folder create a new folder structure that matches the following:

    app/
        lib/
            API/
                Snacks/
                      v1/
                Entity/
                      v1/
                        
                          
                          
Under app/lib/API/Snacks/v1/ you would create a file named SnacksAPI.php. This will be the version 1 of your Snacks API end point.

Entity consists of eloquent models. The reason I use the Entity folder isntead of placing them in the default model directory is, we may need multiple versions of the models for each version of the API. For instance say we need a new table for Snacks for version2, but we don't want to modify the current table that is being used in v1 since it's already in production. We can create another table for the Snack model, and create a second version of the Snack model to load into the v2 of the SnackAPI end point.

Once you have the structure above created, open your project's composer.json file and under the classmap section add:

    "app\lib"

Then run:

    composer dump-autoload

If you had multiple end points you would create multiple directories with the same naming convention as Snacks. The structure of your SnacksAPI.php class should implement the ResourceAPIInterface.php file and should look similar to the below:

    <?php

      namespace API\Snack\v1;

      use API\Resource\v1\ResourceAPIInterface;

      use API\Entity\v1\Snack;

      class SnackAPI implements ResourceAPIInterface {

        const VERSION = '1';
        const NAME='Snack';

        public static function version() {
            return self::VERSION;
        }

        public static function name(){
            return self::NAME;
        }
    
    	public static function fetchOne($id, $data){
        	
        }

        public static function fetchAll($data){

        }

        public static function patch($id, $data){
        	
        }

        public static function update($id, $data){
        	
        }

        public static function create($data){
        	
        }

        public static function deleteOne($id){
        	
        }

        public static function toggle($id, $fieldName='active') {
            
        }
      }

Once you have created the new folders and files, you will need to run the following command from your projects root folder:

    php artisan dump-autoload
    
Each time you add a new end point, make sure you add that end point and it's current version into the restfulapi config file that we published earlier to /app/config/packages/drparham/restfulapi/api.php. Also make sure you run the above command every time you add a new class.


Instructions:
=============

This package creates a group route that will capture anything in the format of api/v#/endpoint and will dynamically determine if there is an endpoint with that version, and if so inject that depedency into a ResourceController that then routes traffic to the specific EndPointAPI.php file in the /app/lib/API/EndPoint/v1/ folder structure.

To add new end points, all you need to do is create the EndPointAPI.php class in the folder structure above, and add it to the api.php config file, with current version. If you only have a v1, and you request a v2, it will fall back to the v1 of the API. This will allow for graceful degredation. If your current version is v2 however it will load the Class that is located in the v2 folder instead of the v1 folder.


### Note::

Currently, I haven't figured out a clean way to determine camelcase end points, so ATM only single word endpoints will load automatically. Such as Snacks, Fruit, Veggies, User, Resource. If you have an end point like:

    https://yourdomain.com/api/v1/smallfruit/
  
The system will be looking for the file Smallfruit/v1/SmallfruitAPI.php, not SmallFruit/v1/SmallFruitAPI.php. 

The later is of course desirable format, however not sure the best way to translate that dynmacially. If you want to add manual translations, you can find the binding here:
      
    /vendor/drparham/restfulapi/src/Drparham/Restfulapi/RestfulapiServiceProvider.php starting on line 38.

You can comment out line 38, and uncomment the lines below and add what ever endpoint translations you want. The major downside here is if there is an update to this package, updating it via composer update will overwrite your modifications. 

### License
This package is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
  
